class MyUser{

  final String uid;
  final bool isEmailVerified;

  MyUser({this.uid, this.isEmailVerified});
}