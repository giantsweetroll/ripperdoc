import 'package:geolocator/geolocator.dart';

class LocationEntry{
  final String placeName;
  final String placeRating;
  final double placeLat;
  final double placeLng;
  final String placeDistance;
  final String time = DateTime.now().toString();

  LocationEntry({
    this.placeName,
    this.placeRating,
    this.placeLat,
    this.placeLng,
    this.placeDistance,

});

  Map<String,dynamic> toMap(){
    return {
      'placeName':    placeName,
      'placeRating':  placeRating,
      'placeLat':     placeLat,
      'placeLng':     placeLng,
      'placeDistance':placeDistance,
      'time': time,

    };
  }

  factory LocationEntry.fromMap(Map<String,dynamic> map){
    return LocationEntry(
        placeName:map['placeName'],
        placeRating:map['placeRating'],
        placeLat:map['placeLat'],
        placeLng:map['placeLng'],
        placeDistance:map['placeDistance'],
    );
  }


}