import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:geolocator/geolocator.dart';
import 'package:provider/provider.dart';
import 'package:ripperdoc_frontend/shared/methods.dart';
import 'package:ripperdoc_frontend/widgets/NavDrawer.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import "package:flutter/material.dart";
import 'package:http/http.dart';
import 'package:ripperdoc_frontend/services/api_services.dart';
import 'package:ripperdoc_frontend/widgets/LocationScreen.dart';
import 'package:image/image.dart' as conv ;
import 'package:image_picker_web/image_picker_web.dart';
import 'package:ripperdoc_frontend/services/locationData.dart' as global;
import 'package:ripperdoc_frontend/widgets/models/user.dart';


import '../shared/constants.dart';

class PhotoScreen extends StatefulWidget {
  @override
  _PhotoScreenState createState() => _PhotoScreenState();
}

class _PhotoScreenState extends State<PhotoScreen> {
  //Fields
  Uint8List imageFile;
  String instructionText = "Upload an Image",
      buttonText = "SEARCH";
  final _formKey = GlobalKey<FormState>();
  TextEditingController textEditingController = TextEditingController();
  String _error = "";
  final double uploadAreaSize = 300;
  final Color uploadImageBoxColor = Colors.black45;
  bool isSubmitPicture = true; // State whether to upload image to backend or to search brand


  @override
  void initState() {
    // TODO: implement initState
    super.initState();

  }

  //Private methods
  void resetFields() {
    this.setState(() {
      this._error = "";
      this.textEditingController.text = "";
      this.imageFile = null;
    });
  }

  Future<void> _pickImage() async {
    this.imageFile =
    await ImagePickerWeb.getImage(outputType: ImageType.bytes);
  }

  Widget _createUploadImageWidget() {
    return DottedBorder(
      color: Colors.black,
      strokeWidth: 2,
      dashPattern: [6, 3, 6, 3],
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(
            child: RawMaterialButton(
              // fillColor: Colors.grey[200],
              // shape: RoundedRectangleBorder(),
              onPressed: () async {
                await this._pickImage();
                this.setState(() {});
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.file_upload,
                    color: Colors.white,
                    size: 90,
                  ),
                  Center(
                    child: Text(
                      this.instructionText,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 32,
                          color: Colors.white
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _showImageWidget() {
    return GestureDetector(
      onTap: () async {
        await this._pickImage();
        this.setState(() {});
      },
      child: Card(
        // elevation: 2.0,
        color: this.uploadImageBoxColor,
        shadowColor: Colors.transparent,
        child: Container(
          padding: EdgeInsets.all(5.0),
          child: Image.memory(
            this.imageFile,
            fit: BoxFit.scaleDown,
          ),
        ),
      ),
    );
  }

  /// Create the main widget of the screen
  Widget _createMainWidget() {
    return Form(
      key: this._formKey,
      child: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Padding(
              padding: EdgeInsets.all(20.0),
              child: Container(
                height: this.uploadAreaSize,
                width: this.uploadAreaSize,
                color: this.imageFile == null
                    ? this.uploadImageBoxColor
                    : Colors.transparent,
                child: this.imageFile == null
                    ? this._createUploadImageWidget()
                    : this._showImageWidget(),
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            Row(
              children: [
                Expanded(
                  child: Container(
                      margin: const EdgeInsets.only(left: 10.0, right: 20.0),
                      child: Divider(
                        color: Colors.black,
                        height: 36,
                      )),
                ),
                Text(
                  "or",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                  ),
                ),
                Expanded(
                  child: Container(
                      margin: const EdgeInsets.only(left: 10.0, right: 20.0),
                      child: Divider(
                        color: Colors.black,
                        height: 36,
                      )),
                ),
              ],
            ),
            SizedBox(height: (20),),
            Padding(
              padding: const EdgeInsets.fromLTRB(50, 0, 50, 30),
              child: TextFormField(
                decoration: InputDecoration(
                  hintText: "Enter brand",
                  hintStyle: TextStyle(
                    color: Colors.white,
                  ),
                  fillColor: Colors.black45,
                  filled: true,
                ),
                style: TextStyle(
                  color: Colors.white,
                ),
                controller: this.textEditingController,
                validator: (val) {
                  if (val.trim().isEmpty) {
                    this.isSubmitPicture = true;
                    return null;
                  } else {
                    this.isSubmitPicture = false;
                    return null;
                  }
                },
              ),
            ),
            Center(
              child: Text(
                this._error,
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.red),
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ElevatedButton(
                  onPressed: this.resetFields,
                  style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.resolveWith((
                          states) => Colors.red)
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 10.0, vertical: 8.0),
                    child: Text(
                      "RESET",
                      style: TextStyle(
                        fontSize: 30,
                      ),
                    ),
                  ),
                ),
                SizedBox(width: 20,),
                ElevatedButton(
                  onPressed: () async {
                    // Validate input
                    this._formKey.currentState.validate();
                    if (this.isSubmitPicture && this.imageFile != null) {
                      this.setState(() {
                        this._error = "";
                      });
                      showLoading(context);
                      await Future.delayed(Duration(seconds: 1));

                      //Convert To JPG
                      try {
                        String base64String = base64.encode(this.convertToJpg(this.imageFile));

                        // Post request to backend
                        try {
                          MyUser myUser = Provider.of<MyUser>(context, listen: false);
                          Response r = await postImage(
                            myUser.uid,
                            base64String,
                          );
                          if (r.statusCode != 200) {
                            this.setState(() {
                              this._error =
                              "Unable to process image due to an error (${r
                                  .statusCode})";
                            });
                            Navigator.pop(context);
                          } else {
                            final body = json.decode(r.body);
                            String logo = body['result'];
                            print(logo);
                            String query = "$logo repair shop";
                            await getCurrentLocation();
                            await searchNearby(query);
                            await getDistance();
                            Navigator.pushReplacement(context, MaterialPageRoute(
                                builder: (context) {
                                  return LocationScreen(query);
                                }));

                          }
                        } catch (e, stacktrace) {
                          print(e);
                          print(stacktrace);
                          this.setState(() {
                            this._error = e;
                            Navigator.pop(context);
                          });
                        } //put it back
                      }
                      catch(e, stacktrace) {
                        this.setState(() {
                          this._error = "Unable to process image due to an error: $e";
                          Navigator.pop(context);
                        });
                      }
                    } else if (!this.isSubmitPicture) {
                      String query = "${this.textEditingController.text} repair shop";
                      showLoading(context);
                      await getCurrentLocation();
                      await  searchNearby(query);
                      await getDistance();
                      Navigator.pushReplacement(context, MaterialPageRoute(
                          builder: (context) => LocationScreen(query)));
                    } else {
                      this.setState(() {
                        this._error = this.isSubmitPicture && this.textEditingController.text.isEmpty
                            ? "Please upload a photo or fill in the text field"
                            : "Please upload a photo";
                      });
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 10.0, vertical: 8.0),
                    child: Text(
                      this.buttonText,
                      style: TextStyle(
                        fontSize: 30,
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
          ],
        ),
      ),
    );
  }

  List<int> convertToJpg(Uint8List image, {int quality:100}) {
    var decodeImage = conv.decodeImage(image);
    return conv.encodeJpg(decodeImage, quality: quality);
  }
  Future<void> getCurrentLocation() async {
    Position position = await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    global.current_latitude = position.latitude.toDouble();
    global.current_longitude = position.longitude.toDouble();
  }

  Future<void> getDistance() async {
    for (var i = 0; i<global.placeLng.length; i++){
      double temp= await Geolocator.distanceBetween(global.placeLat[i], global.placeLng[i], global.current_latitude, global.current_longitude)/1000;
      global.placeDistance.add(temp.toStringAsFixed(2));
    }
  }

  Future<void> searchNearby (String keyword) async{

    String radius = '2000';
    const apiKey = 'AIzaSyDfIMDH961iYy4jRN1yxRDKrydHAC7WRDA';
    try {
      Response r = await postLocation(apiKey,global.current_latitude,global.current_longitude,keyword,radius);    // TODO: change ID according to cloud firestore ID
      final data = jsonDecode(r.body);
      dynamic results = data['data']['results'];
      for( var i = 0 ; i<results.length; i++ ) {
        global.placeName.add(results[i]['name'].toString());
        global.placeRating.add(results[i]['rating'].toString());
        global.placeLat.add(results[i]['geometry']['location']['lat'].toDouble());
        global.placeLng.add(results[i]['geometry']['location']['lng'].toDouble());
      }
    } catch (e, stacktrace) {
      print(e);
      print(stacktrace);

    }

  }

  //Overridden Methods
  @override
  void dispose() {
    this.textEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Row(
          children: [
            CircleAvatar(
              maxRadius: 25,
              backgroundColor: Colors.transparent,
              child: Image.asset('assets/logo.png'),
            ),
            SizedBox(width: 10,),
            Text("RipperDoc"),
          ],
        )
      ),
      drawer: NavDrawer(),
      body: SafeArea(
        child: Container(
          decoration: decorationAppBase,
          child: Center(
            child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Center(
                child: kIsWeb ? SizedBox(
                  // color: colorAppBase,
                  width: this.uploadAreaSize,
                  child: this._createMainWidget(),
                ) : MediaQuery
                    .of(context)
                    .orientation == Orientation.portrait ? this
                    ._createMainWidget() : SizedBox(
                  width: this.uploadAreaSize + 200,
                  child: this._createMainWidget(),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}


