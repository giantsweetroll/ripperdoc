import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import "package:flutter/material.dart";
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter_google_maps/flutter_google_maps.dart';
import 'package:ripperdoc_frontend/widgets/NavDrawer.dart';
import 'package:geolocator/geolocator.dart';
import 'package:ripperdoc_frontend/services/api_services.dart';
import 'package:http/http.dart';
import 'package:ripperdoc_frontend/widgets/models/LocationEntry.dart';
import 'package:ripperdoc_frontend/services/locationData.dart'as global;



class HistoryScreen extends StatefulWidget {

  // Fields


  // Constructor
  HistoryScreen();

  // Overridden Methods
  @override
  _HistoryScreenState createState() => _HistoryScreenState();
}

class _HistoryScreenState extends State<HistoryScreen> {

  //Fields
  final dataCollection = FirebaseFirestore.instance.collection('datas').orderBy('time',descending: true).snapshots();




  String searchKeyword;
  String testing;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final _key = GlobalKey<GoogleMapStateBase>();
  bool _polygonAdded = false;
  bool _darkMapStyle = false;
  String _mapStyle;



  List<String> placeName = ['Dealer Resmi ASUS'];
  List<String> placeRating=['4.1'];
  List<double> placeLat=[-6.223891999999999];
  List<double> placeLng=[106.827181];
  double current_latitude = global.current_latitude;
  double current_longitude= global.current_longitude;
  List<String> placeDistance;


  // Constructor
  _HistoryScreenState();

  // Methods





  List<Widget> getMainWidgets() {
    return [
      Expanded(
          flex: 2,
          child:  Stack(
            children: <Widget>[
              Positioned.fill(
                child: GoogleMap(
                  key: _key,
                  markers: {
                    Marker(
                      GeoCoord(this.current_latitude, this.current_longitude),
                    ),
                  },
                  initialZoom: 12,
                  initialPosition: GeoCoord(this.current_latitude, this.current_longitude), // Los Angeles, CA
                  mapType: MapType.roadmap,
                  mapStyle: _mapStyle,
                  interactive: true,
                  webPreferences: WebMapPreferences(
                    fullscreenControl: true,
                    zoomControl: true,
                  ),
                ),
              ),
              Positioned(
                top: 16,
                right: kIsWeb ? 60 : 16,
                child: FloatingActionButton(
                  onPressed: () {
                    if (_darkMapStyle) {
                      GoogleMap.of(_key).changeMapStyle(null);
                      _mapStyle = null;
                    } else {
                      GoogleMap.of(_key).changeMapStyle(darkMapStyle);
                      _mapStyle = darkMapStyle;
                    }

                    setState(() => _darkMapStyle = !_darkMapStyle);
                  },
                  backgroundColor: _darkMapStyle ? Colors.black : Colors.white,
                  child: Icon(
                    _darkMapStyle ? Icons.wb_sunny : Icons.brightness_3,
                    color: _darkMapStyle ? Colors.white : Colors.black,
                  ),
                ),
              ),
            ],
          )),

      Expanded(child:
      StreamBuilder<QuerySnapshot>(
        stream: dataCollection,
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) {
            return Text('Something went wrong');
          }

          if (snapshot.connectionState == ConnectionState.waiting) {
            return Text("Loading");
          }

          return new ListView(
            children: snapshot.data.docs.map((DocumentSnapshot document) {
              Map<String, dynamic> data = document.data() as Map<String, dynamic>;
              return new ListTile(
                title: new Text(data['placeName'],
                style: TextStyle(fontWeight: FontWeight.bold)),
                subtitle: new Text(data['placeDistance']+"km\n"+data['placeRating']+"\u{2B50}"),
                leading: Icon(Icons.pin_drop),
                onTap: () async {
                  GoogleMap.of(_key).clearDirections();
                  GoogleMap.of(_key).clearMarkers();
                  GoogleMap.of(_key).addDirection( //Direction Function
                    GeoCoord(current_latitude, current_longitude),
                    GeoCoord(data['placeLat'],
                        data['placeLng']),
                    startLabel: '\u{1F464}',
                    endLabel: '\u{1F6E0}',
                  );
                }
              );
            }).toList(),
          );
        },
      )
      ),

    ];
  }


  @override
  void initState(){
    super.initState();

  }

  // Overridden Methods
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          children: [
            CircleAvatar(
              maxRadius: 25,
              backgroundColor: Colors.transparent,
              child: Image.asset('assets/logo.png'),
            ),
            SizedBox(width: 10,),
            Text("RipperDoc"),
          ],
        ),
      ),
      body: OrientationBuilder(
        builder: (context, orientation) {
          return orientation == Orientation.landscape ? Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: getMainWidgets(),
          ) : Column(
            children: getMainWidgets(),
          );
        },
      ),
    );
  }
}


const darkMapStyle = r'''
[
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#212121"
      }
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#212121"
      }
    ]
  },
  {
    "featureType": "administrative",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "administrative.country",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "administrative.locality",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#bdbdbd"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#181818"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#1b1b1b"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry.fill",
    "stylers": [
      {
        "color": "#2c2c2c"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#8a8a8a"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#373737"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#3c3c3c"
      }
    ]
  },
  {
    "featureType": "road.highway.controlled_access",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#4e4e4e"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "transit",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#000000"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#3d3d3d"
      }
    ]
  }
]
''';