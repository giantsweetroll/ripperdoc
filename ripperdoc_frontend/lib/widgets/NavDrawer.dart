import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:ripperdoc_frontend/shared/methods.dart';
import 'package:ripperdoc_frontend/widgets/HistoryScreen.dart';
import 'package:ripperdoc_frontend/widgets/screens/authenticate/authenticate.dart';
import 'package:ripperdoc_frontend/services/locationData.dart' as global;
import 'package:ripperdoc_frontend/services/auth.dart';


class NavDrawer extends StatelessWidget {

  final AuthService _auth = AuthService();

  @override
  Widget build(BuildContext context) {
    Position position;
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CircleAvatar(
                  maxRadius: 40,
                  backgroundColor: Colors.transparent,
                  child: Image.asset("assets/logo.png"),
                ),
                Text(
                  'RipperDoc',
                  style: TextStyle(color: Colors.grey[100], fontSize: 25),
                ),
                SizedBox(height: 5.0,),
                Text(
                  'Find the suitable repair shop for you',
                  style: TextStyle(color: Colors.grey[300], fontSize: 15),
                ),
              ],
            ),

            decoration: BoxDecoration(
              // color: Colors.blueAccent[400],
              color: Colors.black
            ),
          ),
          ListTile(
            leading: Icon(Icons.history),
            title: Text('History'),
            onTap: () async => {
              position = await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high),
              global.current_latitude = position.latitude.toDouble(),
              global.current_longitude = position.longitude.toDouble(),
              Navigator.of(context).pop(),
              Navigator.push(context, MaterialPageRoute(builder: (context) => HistoryScreen() )),
            },
          ),
          ListTile(
            leading: Icon(Icons.logout),
            title: Text('Sign Out'),
            onTap: () async  {
              showLoading(context);
              _auth.signOut();
              Navigator.pop(context);
              Navigator.of(context).pop();
              Navigator.pushReplacement(context, MaterialPageRoute(builder:(context)=> Authenticate()));
            }
          ),
        ],
      ),
    );
  }
}