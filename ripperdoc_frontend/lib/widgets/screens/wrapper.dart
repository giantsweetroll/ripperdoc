import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ripperdoc_frontend/widgets/PhotoScreen.dart';
import 'package:ripperdoc_frontend/widgets/models/user.dart';
import 'package:ripperdoc_frontend/widgets/screens/authenticate/sign_in.dart';
import 'authenticate/authenticate.dart';


class Wrapper extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    final user = Provider.of<MyUser>(context);
    print(user);

    //return either Home or Authenticate widget
    if (user == null || !user.isEmailVerified){
      return SignIn(mustVerifyEmail: user == null ? false : true,);
    } else {
      return PhotoScreen();
    }
  }
}
