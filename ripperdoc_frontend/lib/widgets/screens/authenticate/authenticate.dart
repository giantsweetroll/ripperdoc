import 'package:flutter/material.dart';
import 'package:ripperdoc_frontend/widgets/screens/authenticate/register.dart';
import 'package:ripperdoc_frontend/widgets/screens/authenticate/sign_in.dart';



class Authenticate extends StatefulWidget {

  final bool mustVerifyEmail;
  Authenticate({this.mustVerifyEmail = false});

  @override
  _AuthenticateState createState() => _AuthenticateState();
}

class _AuthenticateState extends State<Authenticate> {

  bool showSignIn = true;

  void toggleView() {
    setState(() => showSignIn = !showSignIn);
  }

  @override
  Widget build(BuildContext context) {
    if (showSignIn) {
      return SignIn(toggleView : toggleView, mustVerifyEmail: widget.mustVerifyEmail);
    } else {
      return Register(toggleView : toggleView);
    }
  }
}
