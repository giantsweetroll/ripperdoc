import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:ripperdoc_frontend/shared/loading.dart';
import 'package:ripperdoc_frontend/services/auth.dart';
import 'package:ripperdoc_frontend/shared/authenticate_shared/constants.dart';

import 'authenticate.dart';


class Register extends StatefulWidget {

  final Function toggleView;
  Register({this.toggleView});

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {

  final AuthService _auth = AuthService();

  final _formKey = GlobalKey<FormState>();

  bool loading = false;

  final auth = FirebaseAuth.instance;

  User user;


  //state to store field
  String email = '';
  String password = '';
  String error = '';
  @override
  Widget build(BuildContext context) {
    return loading ? Loading() : Scaffold(
        backgroundColor: Colors.brown[100],
        appBar: AppBar(
          backgroundColor: Colors.brown[400],
          elevation: 0,
          title : Text('Sign Up to RipperDoc'),
          actions : <Widget> [
            // ignore: deprecated_member_use
            FlatButton.icon(
              icon : Icon(Icons.person),
              label : Text('Sign In'),
              onPressed : () {
                widget.toggleView();
              },
            )
          ],
        ),
        body : Container(
            padding: EdgeInsets.symmetric(vertical: 20, horizontal: 50),
            child: Form(
              key : _formKey,
              child: Column(
                children: <Widget> [
                  SizedBox(height: 20),
                  TextFormField(
                    decoration : textInputDecoration.copyWith(hintText : 'Email'),
                    validator : (val) => val.isEmpty ? 'Enter an email' : null,
                      onChanged: (val) {
                        setState(() => email = val);
                      }
                  ),
                  SizedBox(height: 20),
                  TextFormField(
                    decoration : textInputDecoration.copyWith(hintText : 'Password'),
                    validator : (val) => val.length < 6 ? 'Please provide at least 6 characters' : null,
                    obscureText: true,
                    onChanged: (val) {
                      setState(() => password = val);
                    },
                  ),
                  SizedBox(height: 20),
                  // ignore: deprecated_member_use
                  RaisedButton(
                      color: Colors.pink[400],
                      child: Text(
                          'Register',
                          style : TextStyle(color : Colors.white)
                      ),
                      onPressed : () async {
                        if (_formKey.currentState.validate()){
                          setState(() => loading = true);
                          dynamic result = await _auth.registerWithEmailAndPassword(email,password);
                          if(result == null){
                            setState(() {
                              error = "Please enter a valid email";
                              loading = false;
                            });
                          }else {
                            user = result;
                            user.sendEmailVerification();
                            Navigator.pushReplacement(context, MaterialPageRoute(builder:(context)=> Authenticate(mustVerifyEmail: true)));
                          }
                        }
                      }
                  ),
                  SizedBox(height:12),
                  Text(
                      error,
                  style: TextStyle(color : Colors.red, fontSize : 14)),
                ],
              ),
            )
        )
    );
  }
}

