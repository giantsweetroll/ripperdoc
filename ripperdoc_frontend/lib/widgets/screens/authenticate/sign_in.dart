import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:ripperdoc_frontend/shared/methods.dart';
import 'package:ripperdoc_frontend/widgets/PhotoScreen.dart';
import 'package:ripperdoc_frontend/services/auth.dart';
import 'package:ripperdoc_frontend/shared/authenticate_shared/constants.dart';

class EmailFieldValidator {
  static String validate(val) {
    return val.isEmpty ? 'Enter an email' : null;
  }
}

class PasswordFieldValidator {
  static String validate(val) {
    return val.isEmpty ? 'Enter a password' : null;
  }
}

class SignIn extends StatefulWidget {

  final Function toggleView;
  final bool mustVerifyEmail;
  SignIn({this.toggleView, this.mustVerifyEmail= false});

  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {

  final AuthService _auth = AuthService();

  final _formKey = GlobalKey<FormState>();
  bool isSignIn = true;
  bool isForgetPassword = false;

  //state to store field
  String email = '';
  String password = '';
  String error = '';
  final auth = FirebaseAuth.instance;

  User user;

  @override
  void initState() {
    super.initState();
    
    if (widget.mustVerifyEmail){
      error = "Please Verify your email ";
    }
  }

  @override
  Widget build(BuildContext context) {
    return this.createWidget(context);
  }

  Widget createWidget(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Container(
            width: 800,
            height: 800,
            decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/bg1.jpg'),
                  fit: BoxFit.cover,
                ),
                boxShadow: [
                  BoxShadow(
                      color: Colors.grey.withOpacity(0.4),
                      spreadRadius: 25,
                      blurRadius: 25,
                      offset: Offset(2, 2)
                  )
                ]
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 30.0, horizontal: 50.0),
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                            begin: Alignment.centerLeft,
                            end: Alignment.centerRight,
                            colors: [
                              // Color.fromRGBO(50, 130, 191, 1),
                              // Color.fromRGBO(56, 189, 146, 1),
                              Colors.black,
                              Colors.black,
                            ]
                        )
                    ),
                    child: Form(
                      key: this._formKey,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          CircleAvatar(
                              backgroundColor: Colors.transparent,
                              minRadius: 60,
                              maxRadius: 90,
                              child: Image.asset("assets/logo.png")
                          ),
                          Text(
                            "RipperDoc",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 40,
                            ),
                          ),
                          SizedBox(height: 50.0,),
                          TextFormField(
                            cursorColor: Colors.white,
                            initialValue: email,
                            decoration : authTextInputDecoration.copyWith(
                              hintText : 'Email',
                              prefixIcon: Padding(
                                padding: const EdgeInsets.fromLTRB(5, 0, 25, 5),
                                child: Icon(
                                  Icons.email_outlined,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                            validator : EmailFieldValidator.validate,
                            onChanged: (val) {
                              setState(() => email = val);
                            },
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                          SizedBox(height: 20),
                          !isForgetPassword ? TextFormField(
                            cursorColor: Colors.white,
                            initialValue: password,
                            decoration : authTextInputDecoration.copyWith(
                              hintText : 'Password',
                              prefixIcon: Padding(
                                padding: const EdgeInsets.fromLTRB(5, 0, 25, 5),
                                child: Icon(
                                  Icons.lock_outline,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                            style: TextStyle(
                              color: Colors.white,
                            ),
                            validator : PasswordFieldValidator.validate,
                            obscureText: true,
                            onChanged: (val) {
                              setState(() => password = val);
                            },
                          ):SizedBox(),
                          SizedBox(height:12),
                          Text(
                              error,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color : Colors.red,
                                fontSize : 14,
                              )
                          ),
                          SizedBox(height: 12),
                          Row(
                            children: [
                              Expanded(
                                child: ElevatedButton(
                                    style: ButtonStyle(
                                      shape: MaterialStateProperty.all(
                                        RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(20.0),
                                          side: BorderSide(
                                            // color: this.isSignIn ? Color.fromRGBO(58, 183, 189, 1) : Color.fromRGBO(59, 88, 154, 1),
                                            color: this.isSignIn ? Colors.cyan : Colors.amber,
                                          ),
                                        ),
                                      ),
                                      backgroundColor: MaterialStateProperty.all(
                                        // this.isSignIn ? Color.fromRGBO(58, 183, 189, 1) : Color.fromRGBO(59, 88, 154, 1)
                                          this.isSignIn ? Colors.cyan : Colors.amber
                                      ),
                                      elevation: MaterialStateProperty.all(0.5),
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(vertical: 15),
                                      child: Text(
                                          !this.isSignIn ? "Register": this.isForgetPassword ? "Search" : "Sign In",
                                          style : TextStyle(
                                            color : Colors.white,
                                            fontSize: 20.0,
                                          )
                                      ),
                                    ),
                                    onPressed : () async {
                                      if (_formKey.currentState.validate()){

                                        //turn bool loading to true
                                        // setState(() => loading = true);
                                        showLoading(context);
                                        if(this.isForgetPassword == true){
                                          try {
                                            await _auth.resetPassword(email);
                                            setState(() {
                                              error = "Please check your inbox for a link to reset your password";
                                              this.isForgetPassword = false;
                                              this.email = "";
                                              this.password = "";
                                              // loading = false;
                                              Navigator.pop(context);
                                            });
                                          } on Exception catch (e) {
                                            print(e);
                                            if (e.toString().contains("found")) {
                                              setState(() {
                                                error =
                                                "There is no account associated with this email";
                                                // loading = false;
                                                Navigator.pop(context);
                                              });
                                            }
                                            else {
                                              setState(() {
                                                error =
                                                "Please enter a valid email address";
                                                // loading = false;
                                                Navigator.pop(context);
                                              });
                                            }
                                          }
                                        }
                                        else if (this.isSignIn) {
                                          dynamic result = await _auth.signInWithEmailAndPassword(email, password);
                                          if(result == null){
                                            setState(() {
                                              error = "Could not sign in with those Credentials";
                                              // loading = false;
                                              Navigator.pop(context);
                                            });
                                          }else {
                                            if(result.emailVerified){
                                              Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => PhotoScreen()));
                                            }
                                            else{
                                              setState(() {
                                                error = "Please check your inbox for a link to verify your email address";
                                                // loading = false;
                                                Navigator.pop(context);
                                              });
                                            }
                                          }
                                        } else {
                                          dynamic result = await _auth.registerWithEmailAndPassword(email, password);
                                          if(result == null){
                                            setState(() {
                                              error = "Please enter a valid email";
                                              // loading = false;
                                              Navigator.pop(context);
                                            });
                                          }else {
                                            user = result;
                                            user.sendEmailVerification();
                                            error = "Please check your inbox for a link to verify your email address";
                                            // loading = false;
                                            Navigator.pop(context);
                                            this.isSignIn = true;
                                            setState(() {});
                                          }
                                        }
                                      }
                                    }
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 20,),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              !this.isForgetPassword ? Text(
                                this.isSignIn ? "Don't have an account? " : "Already have an account? ",
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                              ): SizedBox() ,
                              !this.isForgetPassword ? TextButton(
                                child: Text(
                                  this.isSignIn ? "Sign Up" : "Sign In",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                onPressed: () {
                                  this.setState(() {
                                    this.error = "";
                                    this.isSignIn = !this.isSignIn;
                                    this.email = "";
                                    this.password = "";
                                  });
                                },
                              ):SizedBox(),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(height: 20,),
                              this.isSignIn ? TextButton(
                                style: TextButton.styleFrom(
                                  textStyle: const TextStyle(fontSize: 12),
                                ),
                                onPressed: () async {
                                  this.setState(() {
                                    !this.isForgetPassword ? this.isForgetPassword = true : this.isForgetPassword = false;
                                  });
                                },
                                child: !this.isForgetPassword ? Text('Forgot Password') : Text('Go Back'),
                              ) : SizedBox(),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20 ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          "Find the suitable repair shop for you.",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 36,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(height: 15,),
                        Text(
                          "Powered by AI.",
                          style: TextStyle(
                            fontSize: 25,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
