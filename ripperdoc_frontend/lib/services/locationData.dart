library globals;

List<String> placeName =[];
List<String> placeRating=[];
List<double> placeLat=[];
List<double> placeLng=[];
double current_latitude;
double current_longitude;
List<String> placeDistance=[];

void clear(){
  placeName.clear();
  placeRating.clear();
  placeLat.clear();
  placeLng.clear();
  placeDistance.clear();
}