import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:ripperdoc_frontend/shared/constants.dart';

/// POST image into backend server
///
/// userID: the user id from firebase
/// base64String: the image bytes string encoded in Base64 format
Future<http.Response> postImage(String userID, String base64String) {
  return http.post(
    testingMode ? Uri(
        scheme: "http",
        host: domainName,
        port: 5000,
        path: "/$aiSubDomain/$userID"
    ) : Uri(
        scheme: "https",
        host: domainName,
        path: "/$aiSubDomain/$userID"
    ),
    headers: <String, String> {
      'Content-Type' : 'application/json; charset=UTF-8'
    },
    body: jsonEncode(<String, String> {
      "image" : base64String,
    }),
  );
}

/// GET the identified logo served from backend server.
///
/// userID; the user id from firebase
Future<http.Response> getLogoLabel(String userID) {
  return http.get(
    testingMode ? Uri(
        scheme: "http",
        host: domainName,
        port: 5000,
        path: "/$aiSubDomain/$userID"
    ) : Uri(
        scheme: "https",
        host: domainName,
        path: "/$aiSubDomain/$userID"
    )
  );
}

Future<http.Response> postLocation(String apiKey, double latitude, double longitude, String searchKeyWord, String radius) {
  return http.post(
    testingMode? Uri(
        scheme: "http",
        host: domainName,
        port: 5000,
        path: "/$nearbySubDomain",
    ) : Uri(
      scheme: "https",
      host: domainName,
      path: "/$nearbySubDomain",
    ),
    headers: <String, String> {
      'Content-Type' : 'application/json; charset=UTF-8'
    },
    body: jsonEncode(<String, dynamic> {
      "payload": {
        "key": apiKey,
        "location": "$latitude,$longitude",
        "radius": radius,
        "keyword": searchKeyWord
      },
    }),
  );
}



