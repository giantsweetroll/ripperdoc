import 'package:firebase_auth/firebase_auth.dart';
import 'package:ripperdoc_frontend/widgets/models/user.dart';

class AuthService {

  final FirebaseAuth _auth = FirebaseAuth.instance;

  // create user obj based on FirebaseUser
  MyUser _userFromFirebaseUser (User user){
    return user != null ? MyUser(uid: user.uid, isEmailVerified: user.emailVerified) : null;
  }

  //auth change user stream
  Stream<MyUser> get user{
    return _auth.authStateChanges()
        .map(_userFromFirebaseUser);
  }


  //sign in with email and password
  Future signInWithEmailAndPassword(String email, String password) async {
    try{
      UserCredential result = await _auth.signInWithEmailAndPassword(email: email, password: password);
      return result.user;

      //return _userFromFirebaseUser(user);

    }catch (e){
      print(e.toString());
      return null;
    }
  }
  //register with email and password
  Future registerWithEmailAndPassword(String email, String password) async {
    try{
      UserCredential result = await _auth.createUserWithEmailAndPassword(email: email, password: password);
      return result.user;

      //return _userFromFirebaseUser(user);

    }catch (e){
      print(e.toString());
      return null;
    }
  }
  //sign out
  Future signOut() async{
    try{
      return await _auth.signOut();
    }catch(e) {
      print(e.toString());
      return null;
    }
  }

  @override
  Future<void> resetPassword(String email) async {
    await _auth.sendPasswordResetEmail(email: email);
  }

}