import "package:flutter/material.dart";
import 'package:flutter/foundation.dart' show kIsWeb;

const bool testingMode = String.fromEnvironment("TESTING_MODE", defaultValue: "false") == 'true';
// const String domainName = kIsWeb ? "localhost" : "10.0.2.2";
const String domainName = testingMode? kIsWeb ? "localhost" : "10.0.2.2" : "ripperdoc-backend-knpmdgtk2a-de.a.run.app";
const String aiSubDomain = "api-backend";
const String nearbySubDomain ="api-nearby/";
const int backendPort = 5000;

const Color colorAppBase = Color.fromRGBO(63, 86, 120, 1.0);
const Color colorAppSecondary = Color.fromRGBO(63, 86, 120, 1.0);

const BoxDecoration decorationAppBase = BoxDecoration(
  gradient: LinearGradient(
      begin: Alignment.topCenter,
      end: Alignment.bottomCenter,
      stops: [0.1, 0.9],
      colors: [
        colorAppBase,
        colorAppBase,
      ]
  ),
);

const InputDecoration inputTextDecoration = InputDecoration(
  hintText: "",
  labelText: "",
);