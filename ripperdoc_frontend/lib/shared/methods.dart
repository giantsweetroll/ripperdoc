import "package:flutter/material.dart";
import 'package:ripperdoc_frontend/shared/loading.dart';

void showLoading(BuildContext context) {
  showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) {
        return Loading(
          transparent: true,
        );
      }
  );
}