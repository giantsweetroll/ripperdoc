import 'package:flutter/material.dart';

const textInputDecoration = InputDecoration(
  fillColor : Colors.white,
  filled : true,
  enabledBorder : OutlineInputBorder(
  borderSide : BorderSide(color:Colors.white, width : 0.5),
  ),
  focusedBorder : OutlineInputBorder(
  borderSide : BorderSide(color:Colors.pink, width : 0.5),
  ),
);

const authTextInputDecoration = InputDecoration(
  fillColor: Colors.white,
  hintStyle: TextStyle(
    color: Colors.white,
  ),
  enabledBorder: UnderlineInputBorder(
    borderSide: BorderSide(
      color: Colors.white,
    ),
  ),
  focusedBorder: UnderlineInputBorder(
    borderSide: BorderSide(
      color: Colors.white,
    ),
  ),
  border: UnderlineInputBorder(
    borderSide: BorderSide(
      color: Colors.white,
    ),
  ),
);