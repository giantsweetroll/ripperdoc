

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ripperdoc_frontend/widgets/HistoryScreen.dart';
import 'package:ripperdoc_frontend/widgets/LocationScreen.dart';

import 'package:ripperdoc_frontend/widgets/NavDrawer.dart';
import 'package:ripperdoc_frontend/widgets/PhotoScreen.dart';
import 'package:ripperdoc_frontend/widgets/models/user.dart';
import 'package:ripperdoc_frontend/widgets/screens/wrapper.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:ripperdoc_frontend/services/auth.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  runApp(StreamProvider<MyUser>.value(
    value : AuthService().user,
    initialData : null,
    child: MaterialApp(
      title: "RipperDoc",
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        primaryColor: Colors.black
      ),
      initialRoute: '/home',
      routes: {
        '/home': (context) => Wrapper(),
      },
    )
  ));
}
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavDrawer(),
      appBar: AppBar(
        title: Text('RipperDoc'),
      ),
      body: Center(
        child: Text("Home"),
      ),
    );
  }
}