import 'package:flutter_test/flutter_test.dart';
import 'package:ripperdoc_frontend/widgets/screens/authenticate/sign_in.dart';

void main() {
  test('empty email returns error string', () async {
    var result = EmailFieldValidator.validate("");
    expect(result, "Enter an email");
  }, timeout: Timeout.none);

  test('non-empty email returns null', () async {
    var result = EmailFieldValidator.validate("email");
    expect(result, null);
  }, timeout: Timeout.none);

  test('empty password returns error string', () async {
    var result = PasswordFieldValidator.validate("");
    expect(result, "Enter a password");
  }, timeout: Timeout.none);

  test('non-empty password returns null', () async {
    var result = PasswordFieldValidator.validate("password");
    expect(result, null);
  }, timeout: Timeout.none);
}