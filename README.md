![Rd](/uploads/7b410b9bfa94cd1f549e5dc5652b663c/Rd.png)
# RipperDoc
RipperDoc is an application that allows the user to take a photo (or upload one) and the system would identify the nearest repair service shop related to that product. Artifical neural network is used to identify the user's uploaded image.

This project is the final project for our Web Application and Security course, as well as our Intelligent Systems course.

<details>
<summary>Screenshots</summary>
![ripperdoc_signin](Documentation/RipperDoc Frontend/ripperdoc_signin.PNG)
![ripperdoc_photoscreen](Documentation/RipperDoc Frontend/ripperdoc_photoscreen.PNG)
![ripperdoc_photoscreen_w_image](Documentation/RipperDoc Frontend/ripperdoc_photoscreen_w_image.PNG)
![ripperdoc_photoscreen_w_image](Documentation/RipperDoc Frontend/ripperdoc_map.png)
![ripperdoc_photoscreen_w_image](Documentation/RipperDoc Frontend/ripperdoc_map2.png)
</details>

### Technologies used

* Docker Containers (Images can be found at [Docker Hub](https://hub.docker.com/r/giantsweetroll/ripperdoc))
* Firebase
* Gitlab CI / CD
* Google Cloud Run
* Google Maps
* Kubernetes Engine
* Tensorflow Serving

**Note:** This repository was migrated from [GitHub](https://github.com/GiantSweetroll/RipperDoc) (which is no longer being actively maintained).

## Pages

* [Using the App](https://gitlab.com/giantsweetroll/ripperdoc/-/wikis/Using-the-App)
* [Installing on your Machine](https://gitlab.com/giantsweetroll/ripperdoc/-/wikis/Installing-on-your-Machine)
* [Frontend](https://gitlab.com/giantsweetroll/ripperdoc/-/wikis/Frontend)
* [Backend](https://gitlab.com/giantsweetroll/ripperdoc/-/wikis/Backend)
* [Testing](https://gitlab.com/giantsweetroll/ripperdoc/-/wikis/Testing)
* [RipperDoc Collector App](https://gitlab.com/giantsweetroll/ripperdoc/-/wikis/RipperDoc-Collector-App)

## The RipperDoc Team
| Member                         | Binusian ID | Role                                 | Repositories                                                                                  |
|--------------------------------|-------------|--------------------------------------|-----------------------------------------------------------------------------------------------|
| Bently Edyson (bentlyedyson)   | 2301894590  | Frontend and Firebase integration    | [GitHub](https://github.com/bentlyedyson) <br> [GitLab](https://gitlab.com/bentlyedyson)      |
| Eric Edgari (Trutina1220)      | 2301902352  | Frontend and Google Maps integration | [GitHub](https://github.com/Trutina1220) <br> [GitLab](https://gitlab.com/Trutina1220)        |
| Gardyan Akbar (GiantSweetroll) | 2301902296  | Backend and neural network training  | [GitHub](https://github.com/GiantSweetroll) <br> [GitLab](https://gitlab.com/giantsweetroll/) |
