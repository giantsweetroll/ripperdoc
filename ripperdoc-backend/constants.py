import os

# image_width = 224
# image_height = 224
image_width = 299
image_height = 299
color_channels = 3
default_batch_size = 16

dataset_loc:str = "datasets/ripperdoc/"
dataset_test_loc:str = "datasets/ripperdoc_test/"
model_loc:str = "L:/For Machine Learning/Project/RipperDoc/models/"

serving_addr = os.environ.get('SERVING_ADDRESS', "https://ripperdoc-serving-knpmdgtk2a-de.a.run.app/")
# serving_addr = "https://ripperdoc-serving-knpmdgtk2a-de.a.run.app/"
serving_pred_path = "v1/models/ripperdoc:predict"

search_nearby_url = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json'
search_nearby_api_key = os.environ.get("MAPS_NEARBY_API", "")

test_id:str = 'ripperdoc_test'

# labels:[] = [
#     'Acer',
#     'Alienware',
#     'Apple',
#     'Asus',
#     'Dell',
#     'Google',
#     'HP',
#     'Intel',
#     'Lenovo',
#     'LG',
#     'Logitech',
#     'Microsoft',
#     'Motorola',
#     'MSI',
#     'Nokia',
#     'Nvidia',
#     'Oppo',
#     'Panasonic',
#     'Razer',
#     'ROG',
#     'Samsung',
#     'Sony',
#     'Toshiba',
#     'VAIO',
#     'Windows',
#     'Xiaomi'
# ]       # The classifications

labels:[] = [
    'Apple',
    'HP',
    'ROG',
]       # The classifications