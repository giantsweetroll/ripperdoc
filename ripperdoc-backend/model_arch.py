import tensorflow as tf
import tensorflow.keras.layers as layers
import constants
import tensorflow.keras as keras
from tensorflow.keras.regularizers import l2
from tensorflow.python.framework.ops import disable_eager_execution
import file_operations

MOBILENETV2 = tf.keras.applications.MobileNet(
    classes=len(constants.labels),
    classifier_activation='softmax',
    weights=None,
)

XCEPTION = tf.keras.applications.Xception(
    include_top=True, 
    weights=None, 
    input_tensor=None,
    input_shape=None,
    pooling=None,
    classes=len(constants.labels),
    classifier_activation='softmax'
)

ALEXNET = keras.models.Sequential([
    keras.layers.Conv2D(filters=96, kernel_size=(11,11), strides=(4,4), activation='relu', input_shape=(227,227,3)),
    keras.layers.BatchNormalization(),
    keras.layers.MaxPool2D(pool_size=(3,3), strides=(2,2)),
    keras.layers.Conv2D(filters=256, kernel_size=(5,5), strides=(1,1), activation='relu', padding="same"),
    keras.layers.BatchNormalization(),
    keras.layers.MaxPool2D(pool_size=(3,3), strides=(2,2)),
    keras.layers.Conv2D(filters=384, kernel_size=(3,3), strides=(1,1), activation='relu', padding="same"),
    keras.layers.BatchNormalization(),
    keras.layers.Conv2D(filters=384, kernel_size=(3,3), strides=(1,1), activation='relu', padding="same"),
    keras.layers.BatchNormalization(),
    keras.layers.Conv2D(filters=256, kernel_size=(3,3), strides=(1,1), activation='relu', padding="same"),
    keras.layers.BatchNormalization(),
    keras.layers.MaxPool2D(pool_size=(3,3), strides=(2,2)),
    keras.layers.Flatten(),
    keras.layers.Dense(4096, activation='relu'),
    keras.layers.Dropout(0.5),
    keras.layers.Dense(4096, activation='relu'),
    keras.layers.Dropout(0.5),
    keras.layers.Dense(len(constants.labels), activation='softmax')
])

def custom_model(input_shape:(), class_count:int):
    model = tf.keras.models.Sequential(name='ripperdoc_custom')
    model.add(layers.Conv2D(16, kernel_size=(5, 5), activation='relu', input_shape=input_shape))
    model.add(layers.Conv2D(32, kernel_size=(5, 5),activation='relu'))
    model.add(layers.Conv2D(64, (5, 5), activation='relu'))
    # Reduce by taking the max of each 2x2 block
    model.add(layers.MaxPooling2D(pool_size=(2, 2)))
    # Dropout to avoid overfitting
    model.add(layers.Dropout(0.25))
    # Flatten the results to one dimension for passing into our final layer
    model.add(layers.Flatten())
    # A hidden layer to learn with
    model.add(layers.Dense(64, activation='relu'))
    # Another dropout
    model.add(layers.Dropout(0.5))
    # Final categorization with softmax
    model.add(layers.Dense(class_count, activation='softmax'))

    return model

def logo_recog(class_count: int):
    return tf.keras.models.Sequential([
        layers.Conv2D(32, kernel_size=(5, 5), activation='relu', input_shape=(224, 224, 3)),
        layers.MaxPooling2D(strides=2),
        layers.Conv2D(32, kernel_size=(5, 5), activation='relu'),
        layers.AveragePooling2D(strides=2),
        layers.Conv2D(64, kernel_size=(5, 5), activation='relu'),
        layers.AveragePooling2D(strides=2),
        layers.Flatten(),
        layers.Dense(64),
        layers.Dense(class_count, activation='softmax')
    ], name='logo_recog')

def transfer_mobilenet(class_count:int):
    base_model = tf.keras.applications.MobileNet(
        input_shape=(224, 224, 3), 
        alpha=1.0, 
        depth_multiplier=1, 
        dropout=0.001,
        include_top=True,
        weights='imagenet',
        input_tensor=None,
        pooling=None,
        classes=1000,
        classifier_activation='softmax'
    )
    
    base_model.trainable = False

    inputs = tf.keras.Input(shape=(224, 224, 3))

    x = base_model(inputs, training=False)

    outputs = tf.keras.layers.Dense(class_count)(x)

    return tf.keras.Model(inputs, outputs)

def transfer_xception(class_count:int):
    base_model = tf.keras.applications.Xception(
        include_top=True,
        weights="imagenet",
        input_tensor=None,
        input_shape=None,
        pooling=None,
        classes=1000,
        classifier_activation="softmax",
    )

    base_model.trainable = False

    inputs = tf.keras.Input(shape=(299, 299, 3))

    x = base_model(inputs, training=False)
    x = tf.keras.layers.GlobalAveragePooling2D()(x)

    outputs = tf.keras.layers.Dense(class_count)(x)

    return tf.keras.Model(inputs, outputs)

def transfer_vgg19(class_count:int):
    base_model = keras.applications.VGG19(
        include_top=True,
        weights="imagenet",
        input_tensor=None,
        input_shape=None,
        pooling=None,
        classes=1000,
        classifier_activation="softmax",
    )

    base_model.trainable = False

    inputs = tf.keras.Input(shape=(224, 224, 3))

    x = base_model(inputs, training=False)
    # x = tf.keras.layers.GlobalAveragePooling2D()(x)

    outputs = tf.keras.layers.Dense(class_count)(x)

    return tf.keras.Model(inputs, outputs)

def alexnet_v2(class_count:int):
    """
    credits: https://www.mydatahack.com/building-alexnet-with-keras/
    """
    model = keras.models.Sequential()

    # 1st Convolutional Layer
    model.add(layers.Conv2D(filters=96, input_shape=(224,224,3), kernel_size=(11,11),\
    strides=(4,4), padding='valid'))
    model.add(layers.Activation('relu'))
    # Pooling 
    model.add(layers.MaxPooling2D(pool_size=(2,2), strides=(2,2), padding='valid'))
    # Batch Normalisation before passing it to the next layer
    model.add(layers.BatchNormalization())

    # 2nd Convolutional Layer
    model.add(layers.Conv2D(filters=256, kernel_size=(11,11), strides=(1,1), padding='valid'))
    model.add(layers.Activation('relu'))
    # Pooling
    model.add(layers.MaxPooling2D(pool_size=(2,2), strides=(2,2), padding='valid'))
    # Batch Normalisation
    model.add(layers.BatchNormalization())

    # 3rd Convolutional Layer
    model.add(layers.Conv2D(filters=384, kernel_size=(3,3), strides=(1,1), padding='valid'))
    model.add(layers.Activation('relu'))
    # Batch Normalisation
    model.add(layers.BatchNormalization())

    # 4th Convolutional Layer
    model.add(layers.Conv2D(filters=384, kernel_size=(3,3), strides=(1,1), padding='valid'))
    model.add(layers.Activation('relu'))
    # Batch Normalisation
    model.add(layers.BatchNormalization())

    # 5th Convolutional Layer
    model.add(layers.Conv2D(filters=256, kernel_size=(3,3), strides=(1,1), padding='valid'))
    model.add(layers.Activation('relu'))
    # Pooling
    model.add(layers.MaxPooling2D(pool_size=(2,2), strides=(2,2), padding='valid'))
    # Batch Normalisation
    model.add(layers.BatchNormalization())

    # Passing it to a dense layer
    model.add(layers.Flatten())
    # 1st Dense Layer
    model.add(layers.Dense(4096, input_shape=(224*224*3,)))
    model.add(layers.Activation('relu'))
    # Add Dropout to prevent overfitting
    model.add(layers.Dropout(0.4))
    # Batch Normalisation
    model.add(layers.BatchNormalization())

    # 2nd Dense Layer
    model.add(layers.Dense(4096))
    model.add(layers.Activation('relu'))
    # Add Dropout
    model.add(layers.Dropout(0.4))
    # Batch Normalisation
    model.add(layers.BatchNormalization())

    # 3rd Dense Layer
    model.add(layers.Dense(1000))
    model.add(layers.Activation('relu'))
    # Add Dropout
    model.add(layers.Dropout(0.4))
    # Batch Normalisation
    model.add(layers.BatchNormalization())

    # Output Layer
    model.add(layers.Dense(class_count))
    model.add(layers.Activation('softmax'))

    return model

def alexnet_v3(class_count:int):
    """
    Credits: https://stackoverflow.com/questions/60120166/overfitting-alexnet-python
    """
    model = keras.models.Sequential()

    Regularizer = l2(0.001)

    # 1st Convolutional Layer
    model.add(layers.Conv2D(filters=96, input_shape=(227,227,3), kernel_size=(11,11), strides=(4,4), padding='valid'))
    model.add(layers.Activation('relu'))
    # Max Pooling
    model.add(layers.MaxPooling2D(pool_size=(2,2), strides=(2,2), padding='valid'))

    # 2nd Convolutional Layer
    model.add(layers.Conv2D(filters=256, kernel_size=(11,11), strides=(1,1), padding='valid'))
    model.add(layers.Activation('relu'))
    # Max Pooling
    model.add(layers.MaxPooling2D(pool_size=(2,2), strides=(2,2), padding='valid'))

    # 3rd Convolutional Layer
    model.add(layers.Conv2D(filters=384, kernel_size=(3,3), strides=(1,1), padding='valid'))
    model.add(layers.Activation('relu'))

    # 4th Convolutional Layer
    model.add(layers.Conv2D(filters=384, kernel_size=(3,3), strides=(1,1), padding='valid'))
    model.add(layers.Activation('relu'))

    # 5th Convolutional Layer
    model.add(layers.Conv2D(filters=256, kernel_size=(3,3), strides=(1,1), padding='valid'))
    model.add(layers.Activation('relu'))
    # Max Pooling
    model.add(layers.MaxPooling2D(pool_size=(2,2), strides=(2,2), padding='valid'))

    # Passing it to a Fully Connected layer
    model.add(layers.Flatten())
    # 1st Fully Connected Layer
    model.add(layers.Dense(4096, input_shape=(224*224*3,)))
    model.add(layers.Activation('relu'))
    # Add Dropout to prevent overfitting
    model.add(layers.Dropout(0.5))

    # 2nd Fully Connected Layer
    model.add(layers.Dense(4096))
    model.add(layers.Activation('relu'))
    # Add Dropout
    model.add(layers.Dropout(0.5))

    # 3rd Fully Connected Layer
    model.add(layers.Dense(1000))
    model.add(layers.Activation('relu'))
    # Add Dropout
    model.add(layers.Dropout(0.5))

    # Output Layer
    model.add(layers.Dense(class_count))
    model.add(layers.Activation('softmax'))

    # model.add(layers.Conv2D(96, kernel_size=(11, 11), input_shape = (227,227,3),strides=(4,4), padding='valid', activation='relu', data_format='channels_last', 
    #     activity_regularizer=Regularizer, kernel_regularizer=Regularizer))

    # model.add(layers.Dense(units = class_count, activation = 'sigmoid', 
    #     activity_regularizer=Regularizer, kernel_regularizer=Regularizer))


    model.summary()
    return model

def transferred_oxnn_alexnet(class_count:int):
    base_model = file_operations.load_model(constants.model_loc + "OXNN AlexNet/bvlcalexnet-9.onnx")

    base_model.trainable = False
    disable_eager_execution()

    inputs = tf.keras.Input(shape=(224, 224, 3))

    x = base_model(inputs, training=False)

    outputs = tf.keras.layers.Dense(class_count)(x)

    return tf.keras.Model(inputs, outputs)