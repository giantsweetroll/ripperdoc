import unittest
import requests
import constants
import os

class TestDocker(unittest.TestCase):

    backend_url = 'http://localhost:5000/'
    # serving_url = 'http://localhost:8501/'
    serving_url = constants.serving_addr

    # backend_url = 'http://0.0.0.0:5000/'
    # serving_url = 'http://0.0.0.0:8501/'

    # backend_url = 'http://docker:5000/'
    # serving_url = 'http://docker:8501/'

    # backend_url = 'https://34.126.175.112:5000/'
    # serving_url = 'https://34.126.175.112:8501/'

    def test_exist(self):
        # Test backend landing page
        response = requests.get(url=self.backend_url)
        self.assertEqual(response.status_code, 404)

        # Test backend docs page
        response = requests.get(url=self.backend_url + 'docs/')
        self.assertEqual(response.status_code, 200)

        # Test serving landing page
        response = requests.get(url=self.serving_url)
        self.assertEqual(response.status_code, 404)
    
    # Test post data to backend api route
    def test_ai_post(self):
        # Load test image data
        f = open('./test/test-images.txt')
        image_bytes = f.readlines()
        f.close()

        # Send image via post (correct data)
        payload = {'image' : image_bytes[0]}
        response = requests.post(
            url=self.backend_url + 'api-backend/1',
            json=payload
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn('result', response.json())

        # Send incorrect image data
        payload = {'image' : 'this is incorrect data'}
        response = requests.post(
            url=self.backend_url + 'api-backend/1',
            json=payload
        )
        self.assertEqual(response.status_code, 400)
    
    def test_nearby(self):
        ## Test correct POST data
        # Test all data complete and correct
        payload = {
            # 'key': os.environ.get('MAPS_NEARBY_API', "DUMMY"),
            'location':'37.4219983,-122.084',
            'radius':'2000',
            'keyword':'Apple repair shop',
        }
        response = requests.post(
            url=self.backend_url + 'api-nearby/',
            json={
                'payload' : payload
            }
        )
        self.assertTrue(response.status_code, 200)
        if constants.search_nearby_api_key == None or constants.search_nearby_api_key == "DUMMY":
            self.assertIn("REQUEST_DENIED", response.json()['data']['status'])
        else:
            self.assertNotIn("REQUEST_DENIED", response.json()['data']['status'])

        # Test "keyword" missing in payload body
        payload = {
            # 'key': os.environ.get('MAPS_NEARBY_API', "DUMMY"),
            'location':'37.4219983,-122.084',
            'radius':'2000'
        }
        response = requests.post(
            url=self.backend_url + 'api-nearby/',
            json={
                'payload' : payload
            }
        )
        self.assertTrue(response.status_code, 200)
        if constants.search_nearby_api_key == None or constants.search_nearby_api_key == "DUMMY":
            self.assertIn("REQUEST_DENIED", response.json()['data']['status'])
        else:
            self.assertNotIn("REQUEST_DENIED", response.json()['data']['status'])

        ## Test incorrect POST data
        # # Wrong API Key
        # payload = {
        #     'key':'dummyAPIKEy',
        #     'location':'37.4219983,-122.084',
        #     'radius':'2000',
        #     'keyword':'Apple repair shop',
        # }
        # response = requests.post(
        #     url=self.backend_url + 'api-nearby/',
        #     json={
        #         'payload' : payload
        #     }
        # )
        # self.assertTrue(response.status_code, 200)
        # self.assertIn('REQUEST_DENIED', response.json()['data']['status'])

        # # Missing API Key in body
        # payload = {
        #     'location':'37.4219983,-122.084',
        #     'radius':'2000',
        #     'keyword':'Apple repair shop',
        # }
        # response = requests.post(
        #     url=self.backend_url + 'api-nearby/',
        #     json={
        #         'payload' : payload
        #     }
        # )
        # self.assertTrue(response.status_code, 200)
        # self.assertIn('REQUEST_DENIED', response.json()['data']['status'])

        # Missing radius in body
        payload = {
            # 'key':os.environ.get('MAPS_NEARBY_API', "DUMMY"),
            'location':'37.4219983,-122.084',
            'keyword':'Apple repair shop',
        }
        response = requests.post(
            url=self.backend_url + 'api-nearby/',
            json={
                'payload' : payload
            }
        )
        self.assertTrue(response.status_code, 200)
        if constants.search_nearby_api_key == None or constants.search_nearby_api_key == "DUMMY":
            self.assertIn('REQUEST_DENIED', response.json()['data']['status'])
        else:
            self.assertIn('INVALID_REQUEST', response.json()['data']['status'])

        # Missing location in body
        payload = {
            # 'key':os.environ.get('MAPS_NEARBY_API', "DUMMY"),
            'radius':'2000',
            'keyword':'Apple repair shop',
        }
        response = requests.post(
            url=self.backend_url + 'api-nearby/',
            json={
                'payload' : payload
            }
        )
        self.assertTrue(response.status_code, 200)
        if constants.search_nearby_api_key == None or constants.search_nearby_api_key == "DUMMY":
            self.assertIn('REQUEST_DENIED', response.json()['data']['status'])
        else:
            self.assertIn('INVALID_REQUEST', response.json()['data']['status'])

        # Incorrect location in body
        payload = {
            # 'key':os.environ.get('MAPS_NEARBY_API', "DUMMY"),
            'location':'37.4219983',
            'radius':'2000',
            'keyword':'Apple repair shop',
        }
        response = requests.post(
            url=self.backend_url + 'api-nearby/',
            json={
                'payload' : payload
            }
        )
        self.assertTrue(response.status_code, 200)
        if constants.search_nearby_api_key == None or constants.search_nearby_api_key == "DUMMY":
            self.assertIn('REQUEST_DENIED', response.json()['data']['status'])
        else:
            self.assertIn('INVALID_REQUEST', response.json()['data']['status'])

        # Missing both location and radius in body
        payload = {
            # 'key': os.environ.get('MAPS_NEARBY_API', "DUMMY"),
            'keyword':'Apple repair shop',
        }
        response = requests.post(
            url=self.backend_url + 'api-nearby/',
            json={
                'payload' : payload
            }
        )
        self.assertTrue(response.status_code, 200)
        if constants.search_nearby_api_key == None or constants.search_nearby_api_key == "DUMMY":
            self.assertIn('REQUEST_DENIED', response.json()['data']['status'])
        else:
            self.assertIn('INVALID_REQUEST', response.json()['data']['status'])


if __name__ == '__main__':
    unittest.main()