from neural_network import NeuralNetwork
from keras_flops import get_flops

import file_operations
import methods
import constants
import itertools
import time
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import model_arch as m

def evaluate(ai, dataset):
    # Evaluate
    result = ai.evaluate(dataset)

    # Print data
    print("loss:", result[0])
    print("accuracy:", result[1])
    print("F1:", result[2])
    print("Precision:", result[3])
    print("Recall:", result[4])

    return result

def confusion_matrix(model, dataset):
    y_actual = tf.argmax(np.concatenate([y for x, y in dataset], axis=0), axis=-1)
    # y_actual = np.concatenate([y for x, y in dataset], axis=0)

    # Make prediction
    predictions = model.predict(dataset)
    predictions = tf.argmax(predictions, axis=-1)

    # Confusion matrix
    cm = tf.math.confusion_matrix(y_actual, predictions)
    cm = cm/cm.numpy().sum(axis=1)[:, tf.newaxis]

    sns.heatmap(
        cm, annot=True,
        xticklabels=constants.labels,
        yticklabels=constants.labels
    )
    plt.xlabel("Predicted")
    plt.ylabel("True")
    plt.show()

def get_flops_stackoverflow(model_path):
    # Credits: https://stackoverflow.com/questions/49525776/how-to-calculate-a-mobilenet-flops-in-keras
    session = tf.compat.v1.Session()
    graph = tf.compat.v1.get_default_graph()

    with graph.as_default():
        with session.as_default():
            model = file_operations.load_model(model_path)
            print(model.summary())

            run_meta = tf.compat.v1.RunMetadata()
            opts = tf.compat.v1.profiler.ProfileOptionBuilder.float_operation()

            # Optional: save printed results to file
            # flops_log_path = os.path.join(tempfile.gettempdir(), 'tf_flops_log.txt')
            # opts['output'] = 'file:outfile={}'.format(flops_log_path)

            # We use the Keras session graph in the call to the profiler.
            flops = tf.compat.v1.profiler.profile(graph=graph,
                                                  run_meta=run_meta, cmd='op', options=opts)

    tf.compat.v1.reset_default_graph()

    return flops.total_float_ops

def time_taken_to_predict(model, dataset):
    start_time = time.time()
    model.predict(dataset)
    end_time = time.time()
    return f'--- {end_time - start_time} seconds ---'

batch_size:int = 32     # Xception can only handle 32 batch size

# # Load model (1=Logo Recog, 2=AlexNet, 3=Xception)
model = file_operations.load_model("ai/2")

# # Load test dataset
# test_ds = file_operations.load_test_dataset(
#     batch_size=batch_size,
#     img_width=227,
#     img_height=227,
#     grayscale_mode=True
# )

# # Apply test dataset into pipeline
# test_ds = methods.apply_ai_pipeline(
#     test_ds,
#     augment=False,
#     reshuffle_each_iteration=False,
#     convert_as_fake_rgb=True,
#     use_one_hot_format=True,
#     class_count=len(constants.labels),
#     cache_datasets=False,
#     normalize_datasets=True
# )

# # Compile model
# model.compile(optimizer="adam", loss="categorical_crossentropy", metrics=["accuracy", methods.f1_m, methods.precision_m, methods.recall_m])

# Initialize neural network class
# ai = NeuralNetwork(model=model)

# # Evaluate model
# evaluate(ai, test_ds)

# ### Confusion matrix
# confusion_matrix(model, test_ds)

# Calculate flops
# print(get_flops("ai/3"))
flops = get_flops(model, batch_size=1)
print(f"FLOPS: {flops / 10 ** 9:.03} G")

# Calculate execution time
# print(time_taken_to_predict(model, test_ds))

# Model summaries
# def print_model_sumaries():
#     print("Paper")
#     model = file_operations.load_model("ai/1")
#     print(model.summary())
#     print("")

#     print("AlexNet")
#     model = file_operations.load_model("ai/2")
#     print(model.summary())
#     print("")

#     print("Xception")
#     model = file_operations.load_model("ai/3")
#     print(model.summary())
#     print("")

# print_model_sumaries()