from flask import Flask, request
import flask.scaffold
flask.helpers._endpoint_from_view_func = flask.scaffold._endpoint_from_view_func
from flask_restx import Resource, Api, fields
from flask_cors import CORS

import constants
import numpy as np
import requests
import tensorflow as tf
import base64
import io
from PIL import Image
import methods
import os

ai_results = {}     # Dictionary to store the output of the AI

app = Flask(__name__)
CORS(app)
api = Api(app = app,
            version = "1.0",
            title = "RipperDoc API Documentation",
            description = "Upload image of logo to be processed and identified.",
            doc = "/docs/")

ai_name_space = api.namespace('api-backend', description='Manage API for AI')       # Set up name space for AI
nearby_name_space = api.namespace('api-nearby', description="Manage API with Google Maps' Nearby Locations")        # Set up name space for locations nearby

# Model for input from front-end
model_ai = api.model("Send Image",
    {"image" : fields.String(required = True,
        description = "Image bits in Base64 format (jpg)",
        help = "Image cannot be blank")
    }
)

model_nearby = api.schema_model('Get Nearby Locations', {
        'required' : ['payload'],
        'properties' : {
            'payload' : {
                'required' : ['location', 'radius'],
                'properties' : {
                    "location" : {
                        'type' : 'string'
                    },
                    "radius" : {
                        'type' : 'string'
                    },
                    "keyword" : {
                        'type' : 'string'
                    },
                },
                'type' : 'object'
            },
        },
        'type' : 'object'
    }
)

@ai_name_space.route("/<string:id>")
class Home(Resource):
    @api.doc(responses={ 200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error' }, 
			 params={ 'id': 'Specify the user id according to firebase id' })
    def get(self, id):
        if id == constants.test_id:
            return {
                "status" : "Success",
                "statusCode" : "200",
                "result" : "Here have a sweetroll!"
            }
        else:
            # Get the output of the AI
            try:
                return {
                    "status" : "Results retrieved",
                    "statusCode" : "200",
                    "result" : ai_results[id]
                }
            except KeyError as e:
                ai_name_space.abort(500, e.__doc__, status = "Could not retrieve information", statusCode = "500")
            except Exception as e:
                ai_name_space.abort(400, e.__doc__, status = "Could not retrieve information", statusCode = "400")
    
    @api.doc(responses={ 200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error' }, 
			 params={ 'id': 'Specify the Id associated with the person' })
    @api.expect(model_ai)
    def post(self, id):
        if id == 'ripperdoc_test':
            data = request.json['image']
            # print(data)
            return {
                "status" : "Success",
                "statusCode" : "200",
                "result" : data
            }
        else:
            input_arr = None
            try:
                # Decode image into tensor
                image_bytes = request.json['image']
                input_arr = methods.format_image_bytes_to_mlp_input(image_bytes, constants.image_width, constants.image_height)
            except Exception as e:
                ai_name_space.abort(400, __doc__, status = "Could not retrieve information", statusCode = '400')
                return

            # print(input_arr.tolist())

            # Make request to serving docker container
            url = 'http://' + request.remote_addr + ':8501/' + constants.serving_pred_path if os.environ.get('TESTING_MODE', False) == True else constants.serving_addr + constants.serving_pred_path
            # url = constants.serving_addr + constants.serving_pred_path
            # print(url)
            payload = {"instances" : input_arr.tolist()}
            response = requests.post(url = url, json = payload)
            
            if response.ok:
                # Process prediction
                r_json = response.json()        # JSON data of response
                pred_np = np.array(r_json['predictions'])       # Get prediction list and convert it to numpy array
                pred:str = constants.labels[int(pred_np.argmax().__str__())]        # Apply argmax and convert it to label

                # Return as response
                ai_results[id] = pred
                return {
                    "status" : "Image uploaded",
                    "statusCode" : "200",
                    "result" : ai_results[id]
                }
            else:
                ai_name_space.abort(response.status_code, __doc__, status = response.reason, statusCode = f'{response.status_code}')

@nearby_name_space.route("/")
class Nearby(Resource):
    @api.doc(responses={ 200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error' })
    @api.expect(model_nearby)
    def post(self):
        try:
            payload = request.json['payload']
            payload['key'] = constants.search_nearby_api_key

            response = requests.get(
                url=constants.search_nearby_url,
                params=payload
            )

            backend_response = {
                "status" : "Data found",
                "statusCode" : "200",
                "data" : response.json()
            }

            return backend_response

        except KeyError as e:
            print(e)
            nearby_name_space.abort(500, e.__doc__, status = "Could not retrieve information", statusCode = "500")
        except Exception as e:
            print(e)
            nearby_name_space.abort(400, e.__doc__, status = "Could not retrieve information", statusCode = "400")

if __name__ == '__main__':
    # Start flask server
    app.run(host='0.0.0.0')