from neural_network import NeuralNetwork
from random import randrange

import os
import constants
import file_operations
import model_arch as m
import tensorflow.keras as keras
import methods
import tensorflow as tf

# Training params
batch_sizes: list = [32, 64, 128, 256]
initial_learning_rates:list = [1.0, 0.1, 0.01, 0.001, 0.0001, 0.00001, 0.000001, 0.000001]
initial_learning_rates.reverse()      # Reverse it so it starts from lowest :D
loss_functions = [
    "categorical_crossentropy", 
    # "sparse_categorical_crossentropy", 
    "kullback_leibler_divergence"
]
epochs = 50
trials = 1

# Helper functions
def generate_tensorboard_filename(
    model_name:str, 
    loss_function:str,
    optimizer_function:str,
    initial_learning_rate:int,
    batch_size:int,
    trial:int,
):
    return model_name + " loss-" + loss_function + " optimizer-" + optimizer_function + " lr-" + str(initial_learning_rate) + " batch_size-" + str(batch_size) + " t" + str(trial)

def generate_optimizers_list(lr):
    ls = []

    ls.append(keras.optimizers.Adadelta(learning_rate=lr))      # Adadelta
    ls.append(keras.optimizers.Adagrad(learning_rate=lr))       # Adagrad
    ls.append(keras.optimizers.Adam(learning_rate=lr))          # Adam
    ls.append(keras.optimizers.Adamax(learning_rate=lr))        # Adamax
    ls.append(keras.optimizers.Ftrl(learning_rate=lr))          # Ftrl
    ls.append(keras.optimizers.Nadam(learning_rate=lr))         # Nadam
    ls.append(keras.optimizers.RMSprop(learning_rate=lr))       # RMSprop
    ls.append(keras.optimizers.SGD(learning_rate=lr))           # SGD

    ls_name = []

    ls_name.append("Adadelta")
    ls_name.append("Adagrad")
    ls_name.append("Adam")
    ls_name.append("Adamax")
    ls_name.append("Ftrl")
    ls_name.append("Nadam")
    ls_name.append("RMSprop")
    ls_name.append("SGD")

    return ls, ls_name

def train(model_arch, model_name:str, overwrite:bool = True) :
    tensorboard_filename = ""
    csv_filename = ""

    print("Begin training " + model_name + "...")
    for i in range(trials):
        print("Trial:", i)
        model = model_arch
        for batch_size in batch_sizes:
            # Loading dataset
            print('Loading training and validation images....')
            train_ds, val_ds = file_operations.load_training_dataset(augment=True, batch_size=batch_size)
            x_train, y_train = next(iter(train_ds))
            x_val, y_val = next(iter(val_ds))
            print('Training and validation images loaded.')

            # Format dataset if needed
            if model_name.endswith('VGG19'):
                keras.applications.vgg19.preprocess_input(x_train)
                keras.applications.vgg19.preprocess_input(x_val)

            for loss in loss_functions:
                for lr in initial_learning_rates:
                    optimizers, optimizer_names = generate_optimizers_list(lr)
                    for optimizer, optimizer_name in zip(optimizers, optimizer_names):
                        tensorboard_filename = generate_tensorboard_filename(model_name, loss, optimizer_name, lr, batch_size, i)
                        csv_filename = model_name + "/" + generate_tensorboard_filename(model_name, loss, optimizer_name, lr, batch_size, i)
                        model.compile(optimizer, loss, metrics=[
                            "accuracy",
                        ])
                        try:
                            if not os.path.exists('L:/For Machine Learning/Project/RipperDoc/models/' + csv_filename):
                                save_path = constants.model_loc + model_name + "/" + model_name + " loss-" + loss + " optimizer-" + optimizer_name + " lr-" + str(lr) + " batch_size-" + str(batch_size) + " t" + str(i)
                                if not overwrite and not os.path.exists(save_path):
                                    # Train
                                    ai = NeuralNetwork(model=model, tensorboard_file_name=tensorboard_filename, csv_file_name=csv_filename)
                                    ai.train(x_train, y_train, x_val, y_val, batch_size=batch_size, epochs=epochs)
                                    ai.save(save_path)
                        except Exception as e:
                            print("Could not perform training of", tensorboard_filename, 'due to an error')
                            print(e)
    print(model_name + " completed!")

def monitor_dataset(train_ds, val_ds):
    # print("Train dataset count:", tf.data.experimental.cardinality(train_ds).numpy())
    # print("Validation dataset count:", tf.data.experimental.cardinality(val_ds).numpy())

    i = 0
    for _ in train_ds:
        i = i + 1
    print("Training dataset size:", i)
    i = 0
    for _ in val_ds:
        i = i + 1
    print("Validation dataset size:", i)

# constants.image_width = 224
# constants.image_height = 224

# MobileNetV2
# train(m.MOBILENETV2, "MobileNetV2")

# Transfered MobileNet
# train(m.transfer_mobilenet(len(constants.labels)), "Transferred MobileNet")

# Logo Recognition Arch from Paper
# train(m.logo_recog(len(constants.labels)), "Logo Recog")

# Custom Model
# train(m.custom_model((constants.image_width, constants.image_height, constants.color_channels), len(constants.labels)), "RipperDoc Custom")

# Transferred VGG19
# train(m.transfer_vgg19(len(constants.labels)), "Transferred VGG19", overwrite=False)

# AlexNet v2
# train(m.alexnet_v2(len(constants.labels)), "AlexNetV2", overwrite=False)

# Xception
# constants.image_width = 299
# constants.image_height = 299
# train(m.XCEPTION, "Xception")

# Transferred Xception
# train(m.transfer_xception(len(constants.labels)), "Transferred Xception")

# AlexNet
# constants.image_height = 227
# constants.image_width = 227
# train(m.ALEXNET, "AlexNet")

# Load AI
trials: int = 25
batch_size:int = 128
epochs = 30
convert_to_one_hot:bool = True
labels:int = len(constants.labels)
seeds = []
model_name = "AlexNetV4"
# model = m.alexnet_v3(labels)
model = file_operations.load_model(constants.model_loc + model_name + "/" + f'{model_name} Alpha 5')
# model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
# model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'], experimental_run_tf_function=False)

for i in range(trials):
    print("Trial:", i)
    # Loading dataset
    seed = randrange(500)
    seeds.append(seed)
    print('Loading training and validation images....')
    train_ds, val_ds = file_operations.load_training_dataset(batch_size=batch_size, seed=seed, grayscale_mode=True)
    # train_ds, val_ds = keras_datasets.get_cifar_10()
    train_ds = methods.apply_ai_pipeline(
        train_ds,
        augment=True,
        reshuffle_each_iteration=False,
        convert_as_fake_rgb=True,
        use_one_hot_format=True,
        class_count=labels,
        cache_datasets=False,
        normalize_datasets=True
    )
    val_ds = methods.apply_ai_pipeline(
        val_ds,
        augment=True,
        reshuffle_each_iteration=False,
        convert_as_fake_rgb=True,
        use_one_hot_format=True,
        class_count=labels,
        cache_datasets=False,
        normalize_datasets=True
    )
    monitor_dataset(train_ds, val_ds)
    print('Training and validation images loaded.')

    ai = NeuralNetwork(
        model=model,
        tensorboard_file_name=f'{model_name}/{model_name} {i}',
        csv_file_name=f'{model_name}/{model_name}'
    )
    ai.train(
        train_ds,
        val_ds,
        batch_size=batch_size, 
        epochs=epochs,
        use_early_stopping=True,
        append_csv=True
    )
    model_path = constants.model_loc + model_name + "/" + model_name + f' {i}' 
    ai.save(model_path)
    model = file_operations.load_model(model_path)
print('seeds:', seeds)
