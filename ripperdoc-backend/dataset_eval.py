import matplotlib.pyplot as plt
import methods
import constants
import file_operations
import math

def plot_dataset(dataset, count:float, label:str = None):
    plt.figure(figsize=(count+1, count+1))
    # class_names = dataset.class_names
    class_names = constants.labels
    filtered_images:list = []

    # Calculate subplot divisions
    base_root = math.sqrt(count)
    num1 = base_root
    num2 = base_root
    if base_root % math.floor(base_root) != 0:
        num1 = math.floor(base_root)
        num2 = math.ceil(base_root)

    # Filter out dataset for images from specific label
    total = 0
    for images, labels in dataset:
        for i in range(len(labels)):
            if label != None and class_names[labels[i]] == label:
                filtered_images.append(images[i])
                total += 1
                if total == count:
                    break
            elif label != None:
                continue
            else:
                filtered_images.append(images[i])
                total += 1
                if total == count:
                    break
        if total == count:
            break

    for i in range(count):
        ax = plt.subplot(num1, num2, i + 1)
        plt.imshow(filtered_images[i].numpy().astype("uint8"))
        plt.axis("off")
            
    plt.show()

def calc_images_count(dataset):
    count = 0
    for images, labels in dataset:
        for image in images:
            count +=1
    return count

train_ds, val_ds = file_operations.load_training_dataset(
    batch_size=128, 
    img_width=224,
    img_height=224,
    seed=123,
    grayscale_mode=False
)

print("Training dataset before augmentation:", calc_images_count(train_ds), "images")
print("Validation dataset before augmentation:", calc_images_count(val_ds), "images")

train_ds = methods.apply_ai_pipeline(
    train_ds,
    augment=True,
    reshuffle_each_iteration=False,
    convert_as_fake_rgb=False,
    use_one_hot_format=False,
    class_count=len(constants.labels),
    cache_datasets=False,
    normalize_datasets=False
)

val_ds = methods.apply_ai_pipeline(
    val_ds,
    augment=True,
    reshuffle_each_iteration=False,
    convert_as_fake_rgb=False,
    use_one_hot_format=False,
    class_count=len(constants.labels),
    cache_datasets=False,
    normalize_datasets=False
)

print("Training dataset after augmentation:", calc_images_count(train_ds), "images")
print("Validation dataset after augmentation:", calc_images_count(val_ds), "images")

plot_dataset(val_ds, 30, 'ROG')
