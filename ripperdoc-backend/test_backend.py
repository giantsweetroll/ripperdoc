from app import app
import unittest
import constants
import os

class TestBackend(unittest.TestCase):

    def setUp(self):
        self.tester = app.test_client(self)

    # Test that the user can GET to the docs page and the homepage returns a 404 error
    def test_get(self):
        # Test landing page
        response = self.tester.get('/')
        self.assertEqual(response.status_code, 404)

        # Test docs
        response = self.tester.get('docs/')
        self.assertEqual(response.status_code, 200)
    
    # Test that docs page loads correctly
    def test_docs_loads(self):
        response = self.tester.get('docs/')
        self.assertTrue(b'RipperDoc API Documentation' in response.data)

    # Test using GET to access data from AI
    def test_api_get(self):
        user_id:str = '123Z332'
        response = self.tester.get(f'api-backend/{user_id}')
        self.assertTrue(response.status_code, 500)      # Data from page shouldn't exist

        user_id:str = constants.test_id
        response = self.tester.get(f'api-backend/{user_id}')
        self.assertEqual(response.status_code, 200)      # Test data should exist
        self.assertTrue(b'Here have a sweetroll!' in response.data)        # Test there should be 'Here have a sweetroll!' in response using GET

    # Test using POST to AI server
    def test_api_post(self):
        # Test correct POST data
        user_id:str = constants.test_id
        response = self.tester.post(
            f'api-backend/{user_id}',
            json={'image' : 'test-image-baby'}
        )
        self.assertTrue(response.status_code, 200)
        self.assertIn(b'test-image-baby', response.data)

        # Test incorrect POST data
        response = self.tester.post(
            f'api-backend/{user_id}',
            json={'wrong_data_format' : 'test-image-baby'}
        )
        self.assertTrue(response.status_code, 400)

        self.user_id = 'lulululua'
        response = self.tester.post(
            f'api-backend/{user_id}',
            json={'image' : 'wrong_image_data'}
        )
        self.assertTrue(response.status_code, 400)
    
    def test_nearby(self):
        ## Test correct POST data
        # Test all data complete and correct
        payload = {
            # 'key': os.environ.get('MAPS_NEARBY_API', "DUMMY"),
            'location':'37.4219983,-122.084',
            'radius':'2000',
            'keyword':'Apple repair shop',
        }
        response = self.tester.post(
            'api-nearby/',
            json={
                'payload' : payload
            }
        )
        self.assertTrue(response.status_code, 200)
        if constants.search_nearby_api_key == None or constants.search_nearby_api_key == "DUMMY":
            self.assertIn(b"REQUEST_DENIED", response.data)
        else:
            self.assertNotIn(b"REQUEST_DENIED", response.data)

        # Test "keyword" missing in payload body
        payload = {
            # 'key': os.environ.get('MAPS_NEARBY_API', "DUMMY"),
            'location':'37.4219983,-122.084',
            'radius':'2000'
        }
        response = self.tester.post(
            'api-nearby/',
            json={
                'payload' : payload
            }
        )
        self.assertTrue(response.status_code, 200)
        if constants.search_nearby_api_key == None or constants.search_nearby_api_key == "DUMMY":
            self.assertIn(b"REQUEST_DENIED", response.data)
        else:
            self.assertNotIn(b"REQUEST_DENIED", response.data)

        ## Test incorrect POST data
        # # Wrong API Key
        # payload = {
        #     'key':'dummyAPIKEy',
        #     'location':'37.4219983,-122.084',
        #     'radius':'2000',
        #     'keyword':'Apple repair shop',
        # }
        # response = self.tester.post(
        #     'api-nearby/',
        #     json={
        #         'payload' : payload
        #     }
        # )
        # self.assertTrue(response.status_code, 200)
        # self.assertIn(b'REQUEST_DENIED', response.data)

        # # Missing API Key in body
        # payload = {
        #     'location':'37.4219983,-122.084',
        #     'radius':'2000',
        #     'keyword':'Apple repair shop',
        # }
        # response = self.tester.post(
        #     'api-nearby/',
        #     json={
        #         'payload' : payload
        #     }
        # )
        # self.assertTrue(response.status_code, 200)
        # self.assertIn(b'REQUEST_DENIED', response.data)

        # Missing radius in body
        payload = {
            # 'key':os.environ.get('MAPS_NEARBY_API', "DUMMY"),
            'location':'37.4219983,-122.084',
            'keyword':'Apple repair shop',
        }
        response = self.tester.post(
            'api-nearby/',
            json={
                'payload' : payload
            }
        )
        self.assertTrue(response.status_code, 200)
        if constants.search_nearby_api_key == None or constants.search_nearby_api_key == "DUMMY":
            self.assertIn(b"REQUEST_DENIED", response.data)
        else:
            self.assertIn(b"INVALID_REQUEST", response.data)

        # Missing location in body
        payload = {
            # 'key':os.environ.get('MAPS_NEARBY_API', "DUMMY"),
            'radius':'2000',
            'keyword':'Apple repair shop',
        }
        response = self.tester.post(
            'api-nearby/',
            json={
                'payload' : payload
            }
        )
        self.assertTrue(response.status_code, 200)
        if constants.search_nearby_api_key == None or constants.search_nearby_api_key == "DUMMY":
            self.assertIn(b"REQUEST_DENIED", response.data)
        else:
            self.assertIn(b"INVALID_REQUEST", response.data)

        # Incorrect location in body
        payload = {
            # 'key':os.environ.get('MAPS_NEARBY_API', "DUMMY"),
            'location':'37.4219983',
            'radius':'2000',
            'keyword':'Apple repair shop',
        }
        response = self.tester.post(
            'api-nearby/',
            json={
                'payload' : payload
            }
        )
        self.assertTrue(response.status_code, 200)
        if constants.search_nearby_api_key == None or constants.search_nearby_api_key == "DUMMY":
            self.assertIn(b"REQUEST_DENIED", response.data)
        else:
            self.assertIn(b"INVALID_REQUEST", response.data)

        # Missing both location and radius in body
        payload = {
            # 'key': os.environ.get('MAPS_NEARBY_API', "DUMMY"),
            'keyword':'Apple repair shop',
        }
        response = self.tester.post(
            'api-nearby/',
            json={
                'payload' : payload
            }
        )
        self.assertTrue(response.status_code, 200)
        if constants.search_nearby_api_key == None or constants.search_nearby_api_key == "DUMMY":
            self.assertIn(b"REQUEST_DENIED", response.data)
        else:
            self.assertIn(b"INVALID_REQUEST", response.data)

if __name__ == '__main__':
    unittest.main()