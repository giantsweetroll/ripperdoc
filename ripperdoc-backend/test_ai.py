import unittest
import neural_network
import file_operations
from PIL import Image
import base64
import io
import constants

class TestAI(unittest.TestCase):
    
    def test_pred(self):
        print('test_pred')
        
        print('Loading image inputs...')
        f = open('./test/test-images.txt', 'r')
        strings = f.readlines()
        f.close()
        
        print("loading neural network model...")
        nn = neural_network.NeuralNetwork(model=file_operations.load_model('./ai/3'))
        
        print('Begin testing images...')
        for image_bytes in strings:
            image = file_operations.convert_base64_to_image(image_bytes)

            pred = nn.predict(image)
            print('Image predicted as', pred)
            self.assertIn(pred, constants.labels)

if __name__ == '__main__':
    unittest.main()