from PIL import Image

import constants
import tensorflow as tf
import methods
import io
import glob
import os
import base64
from io import BytesIO

def load_training_dataset(
    batch_size:int = constants.default_batch_size,
    img_width:int = constants.image_width, 
    img_height:int = constants.image_height,
    validation_split:float = 0.2,
    seed = 123,
    grayscale_mode:bool = False
):
    """ Load training and validation images using tensorflow"""
    # Get dataset directory
    data_dir:str = constants.dataset_loc     # Dataset directory
    
    # Get training dataset
    train_ds = tf.keras.preprocessing.image_dataset_from_directory(
        data_dir,
        validation_split=validation_split,
        subset='training',
        seed=seed,
        image_size=(img_width, img_height),
        batch_size=batch_size,
        color_mode='grayscale' if grayscale_mode else 'rgb'
    )
    
    # Get validation dataset
    val_ds = tf.keras.preprocessing.image_dataset_from_directory(
        data_dir,
        validation_split=validation_split,
        subset='validation',
        seed=seed,
        image_size=(img_width, img_height),
        batch_size=batch_size,
        color_mode='grayscale' if grayscale_mode else 'rgb'
    )

    return train_ds, val_ds

def load_test_dataset(
    batch_size:int = constants.default_batch_size,
    img_width:int = constants.image_width, 
    img_height:int = constants.image_height,
    grayscale_mode:bool = False
):
    """ Load test images using tensorflow"""
    # Get dataset directory
    data_dir:str = constants.dataset_test_loc     # Test Dataset directory
    
    # Get training dataset
    test_ds = tf.keras.preprocessing.image_dataset_from_directory(
        data_dir,
        image_size=(img_width, img_height),
        batch_size=batch_size,
        color_mode='grayscale' if grayscale_mode else 'rgb'
    )

    return test_ds

def load_model(path:str):
    """
    Method to laod the desired neural network model
    path: the path of the model file
    """
    try:
        return tf.keras.models.load_model(path)
    except IOError as e:
        print(e)
        return None

def load_image(path:str):
    return Image.open(path)

def convert_png_to_jpg(image:Image):
    return image.convert('RGB')

def get_as_base64_from(image: Image):
    img_bytes = io.BytesIO()
    image.save(img_bytes, format="JPEG")

    return base64.b64encode(img_bytes.getvalue())

def convert_base64_to_image(base64str:str):
    base64_bytes = base64str.encode('ascii')
    im_file = BytesIO(base64.b64decode(base64_bytes))

    return Image.open(im_file)

def convert_all_png_into_jpg(directory:str):
    png_images = glob.glob(directory + '/*.png')
    print('found', len(png_images), ' png images...')
    for png_fn in png_images:
        print(png_fn)
        img = load_image(png_fn)
        img = convert_png_to_jpg(img)
        img.save(png_fn[:len(png_fn)-4] + '.jpg')

def delete_all_png(directory:str):
    png_images = glob.glob(directory + '/*.png')
    for png_fn in png_images:
        os.remove(png_fn)

def convert_all_image_from_dataset_to_jpg(directory: str):
    classes = glob.glob(directory + '/*')
    for c in classes:
        convert_all_png_into_jpg(c)
        delete_all_png(c)

# convert_all_image_from_dataset_to_jpg("L:/For Machine Learning/Project/RipperDoc/dataset")
