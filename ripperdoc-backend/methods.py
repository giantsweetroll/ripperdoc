import tensorflow as tf
import io
import base64
from PIL import Image
import numpy as np
import file_operations

from tensorflow.keras import backend as K

def normalize_dataset(ds, normalization_layer = None):
    """ Normalize the dataset """
    if normalization_layer == None:
        normalization_layer = tf.keras.layers.experimental.preprocessing.Rescaling(1./255)
    return ds.map(lambda x, y: (normalization_layer(x), y))

def format_image_bytes_to_mlp_input(image_bytes:str, img_width:int, img_height:int):
    """
    Converts image_bytes from string of bytes into a PIL Image and then convert it into a numpy array.
    Image is resized to the specified size and formalized.
    """
    image = file_operations.convert_base64_to_image(image_bytes)

    # Format input
    input_arr = tf.keras.preprocessing.image.img_to_array(image)
    input_arr = np.array([input_arr])
    input_arr = tf.image.resize(input_arr, [img_width, img_height])  # Resize image to fit neural network input
    input_arr = np.array(input_arr)     # Convert it back to numpy array
    input_arr /= 255        # Apply normalization

    return input_arr

def convert_grayscale_to_rgb(ds):
    rgb_ds = ds.map(lambda x, y : (tf.image.grayscale_to_rgb(x), y))

    return rgb_ds

def apply_data_augmentation(
    ds,
    autotune = tf.data.AUTOTUNE, 
    shuffle:bool=True,
    reshuffle_each_iteration:bool=False,
):
    if shuffle:
        aug_ds = ds.shuffle(1000000, reshuffle_each_iteration=reshuffle_each_iteration)
    
    data_augmentation = tf.keras.Sequential([
            tf.keras.layers.experimental.preprocessing.RandomFlip("horizontal_and_vertical"),
            tf.keras.layers.experimental.preprocessing.RandomRotation(0.2, fill_mode='constant'),
            # tf.keras.layers.experimental.preprocessing.RandomContrast(0.2)
        ])
    aug_ds = ds.map(lambda x, y: (data_augmentation(x, training=True), y), num_parallel_calls=autotune)
    
    return aug_ds

def cache_datasets(ds, autotune = tf.data.AUTOTUNE):
    cached_ds = ds.cache().prefetch(buffer_size=autotune)

    return cached_ds

def convert_to_one_hot_format(ds, class_count):
    return ds.map(lambda x, y : (x, tf.one_hot(y, depth=class_count)))

def apply_ai_pipeline(
    ds, 
    augment:bool = False,
    reshuffle_each_iteration:bool = False,
    convert_as_fake_rgb:bool = False,
    use_one_hot_format:bool = False,
    class_count:int = 0,
    cache_datasets:bool = True,
    normalize_datasets:bool = True
):
    # Convert grayscale images into 3 color channels
    if convert_as_fake_rgb:
        ds = convert_grayscale_to_rgb(ds)
    
    # Normalize data
    if normalize_datasets:
        normalization_layer = tf.keras.layers.experimental.preprocessing.Rescaling(1./255)
        ds = normalize_dataset(ds, normalization_layer)

    # Data augmentation
    if augment:
        ds = apply_data_augmentation(ds, reshuffle_each_iteration=reshuffle_each_iteration)

    # Convert to one hot format
    if use_one_hot_format and class_count > 0:
        ds = convert_to_one_hot_format(ds, class_count)

    # Configure performance
    if cache_datasets:
        ds = cache_datasets(ds)

    return ds

# Metrics functions
# Credits: https://datascience.stackexchange.com/questions/45165/how-to-get-accuracy-f1-precision-and-recall-for-a-keras-model
def recall_m(y_true, y_pred):
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
    recall = true_positives / (possible_positives + K.epsilon())
    return recall

def precision_m(y_true, y_pred):
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
    precision = true_positives / (predicted_positives + K.epsilon())
    return precision

def f1_m(y_true, y_pred):
    precision = precision_m(y_true, y_pred)
    recall = recall_m(y_true, y_pred)
    return 2*((precision*recall)/(precision+recall+K.epsilon()))